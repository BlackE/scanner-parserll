/* 
 * File:   Scanner.cpp
 * Author: blacke
 * 
 * Created on April 11, 2013, 12:40 PM
 */
#include "Scanner.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#define TIPONODO_INTERMEDIO      (0)
#define TIPONODO_INTERMEDIO_FINAL (1)
#define TIPONODO_FINAL           (2)


#define SCANNER_ERROR (-1)
#define SCANNER_TOKEN_RECONOCIDO (-1)
#define AUTOMATA_ESTADOS_NUM (46)                       //total de estados
#define AUTOMATA_SIMBOLOS_ENTRADA_Y_CONTROL (85)        //cadenas esperadas sin contar los ultimos
                                                        //estados finales que son
                                                        // \n , " ", EOF
#define AUTOMATA_TIPO_NODO (AUTOMATA_SIMBOLOS_ENTRADA_Y_CONTROL - 2)    //tipo de nodo
#define AUTOMATA_CODIGO_SALIDA (AUTOMATA_SIMBOLOS_ENTRADA_Y_CONTROL - 1)	//terminal o no

#define SIMB_T_programa (1)
#define SIMB_T_LLAVE_ABRE (2)
#define SIMB_T_LLAVE_CIERRA (3)
#define SIMB_T_PUNTO_Y_COMA (4)
#define SIMB_T_IGUAL (5)
#define SIMB_T_DOBLE_IGUAL (6)
#define SIMB_T_if (7)
#define SIMB_T_PARENTESIS_ABRE (8)
#define SIMB_T_PARENTESIS_CIERRA (9)
#define SIMB_T_else (10)
#define SIMB_T_INTERROGACION (11)
#define SIMB_T_ADMIRACION (12)
#define SIMB_T_DIFERENTE (13)
#define SIMB_T_SUMA (14)
#define SIMB_T_RESTA (15)
#define SIMB_T_MULTIPLICACION (16)
#define SIMB_T_DIVISION (17)
#define SIMB_T_MENOR (18)
#define SIMB_T_MAYOR (19)
#define SIMB_T_MENOR_IGUAL (20)
#define SIMB_T_MAYOR_IGUAL (21)
#define SIMB_T_POTENCIA (22)
#define SIMB_T_DIGITO_ENTERO (23)
#define SIMB_T_DIGITO_DECIMAL (24)
#define SIMB_T_GUION_BAJO (25)
#define SIMB_T_while (26)
#define SIMB_T_ID (27)
#define SIMB_T_ESP (28)
#define SIMB_T_TAB (29)
#define SIMB_T_SALTO_DE_LINEA (30)
#define SIMB_T_EOF (31)




int MT[AUTOMATA_ESTADOS_NUM][AUTOMATA_SIMBOLOS_ENTRADA_Y_CONTROL] = 
{
    
{45,45,45,45,11,45,45,45,9,45,45,45,45,45,45,1,45,45,45,45,45,45,15,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,43,43,43,43,43,43,43,43,43,45,20,21,22,24,25,26,27,28,29,30,31,33,35,37,38,-1,39,40,41,42,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,2,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,3,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,4,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,5,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{6,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,7,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{8,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_programa},
{45,45,45,45,45,10,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_if},
{45,45,45,45,45,45,45,45,45,45,45,12,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,13,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,14,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_else},
{45,45,45,45,45,45,45,16,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,17,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,18,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,19,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_while},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_LLAVE_ABRE},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_LLAVE_CIERRA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,23,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_IGUAL},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_DOBLE_IGUAL},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_PARENTESIS_ABRE},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_PARENTESIS_CIERRA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_SUMA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_RESTA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_MULTIPLICACION},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_DIVISION},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_POTENCIA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,32,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_ADMIRACION},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_DIFERENTE},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,34,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_MENOR},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_MENOR_IGUAL},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,36,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_MAYOR},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_MAYOR_IGUAL},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_PUNTO_Y_COMA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_INTERROGACION},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_ESP},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_TAB},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_SALTO_DE_LINEA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_EOF},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,43,43,43,43,43,43,43,43,43,43,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,44,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_DIGITO_ENTERO},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,44,44,44,44,44,44,44,44,44,44,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_DIGITO_DECIMAL},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_ID},

};

int Scanner::ObtenerCodigoSimboloEntrada(int intASCIISimboloEntrada)
{    
    int intCodigoSimboloEntrada = -1;   
    switch(intASCIISimboloEntrada)
    {
        case 'a' : intCodigoSimboloEntrada = 0; break;
        case 'b' : intCodigoSimboloEntrada = 1; break;
        case 'c' : intCodigoSimboloEntrada = 2; break;
        case 'd' : intCodigoSimboloEntrada = 3; break;
        case 'e' : intCodigoSimboloEntrada = 4; break;
        case 'f' : intCodigoSimboloEntrada = 5; break;
        case 'g' : intCodigoSimboloEntrada = 6; break;
        case 'h' : intCodigoSimboloEntrada = 7; break;
        case 'i' : intCodigoSimboloEntrada = 8; break;
        case 'j' : intCodigoSimboloEntrada = 9; break;
        case 'k' : intCodigoSimboloEntrada = 10; break;
        case 'l' : intCodigoSimboloEntrada = 11; break;
        case 'm' : intCodigoSimboloEntrada = 12; break;
        case 'n' : intCodigoSimboloEntrada = 13; break;
        case 'o' : intCodigoSimboloEntrada = 14; break;
        case 'p' : intCodigoSimboloEntrada = 15; break;
        case 'q' : intCodigoSimboloEntrada = 16; break;
        case 'r' : intCodigoSimboloEntrada = 17; break;
        case 's' : intCodigoSimboloEntrada = 18; break;
        case 't' : intCodigoSimboloEntrada = 19; break;
        case 'u' : intCodigoSimboloEntrada = 20; break;
        case 'v' : intCodigoSimboloEntrada = 21; break;
        case 'w' : intCodigoSimboloEntrada = 22; break;
        case 'x' : intCodigoSimboloEntrada = 23; break;
        case 'y' : intCodigoSimboloEntrada = 24; break;
        case 'z' : intCodigoSimboloEntrada = 25; break;
        case 'A' : intCodigoSimboloEntrada = 26; break;
        case 'B' : intCodigoSimboloEntrada = 27; break;
        case 'C' : intCodigoSimboloEntrada = 28; break;
        case 'D' : intCodigoSimboloEntrada = 29; break;
        case 'E' : intCodigoSimboloEntrada = 30; break;
        case 'F' : intCodigoSimboloEntrada = 31; break;
        case 'G' : intCodigoSimboloEntrada = 32; break;
        case 'H' : intCodigoSimboloEntrada = 33; break;
        case 'I' : intCodigoSimboloEntrada = 34; break;
        case 'J' : intCodigoSimboloEntrada = 35; break;
        case 'K' : intCodigoSimboloEntrada = 36; break;
        case 'L' : intCodigoSimboloEntrada = 37; break;
        case 'M' : intCodigoSimboloEntrada = 38; break;
        case 'N' : intCodigoSimboloEntrada = 39; break;
        case 'O' : intCodigoSimboloEntrada = 40; break;
        case 'P' : intCodigoSimboloEntrada = 41; break;
        case 'Q' : intCodigoSimboloEntrada = 42; break;
        case 'R' : intCodigoSimboloEntrada = 43; break;
        case 'S' : intCodigoSimboloEntrada = 44; break;
        case 'T' : intCodigoSimboloEntrada = 45; break;
        case 'U' : intCodigoSimboloEntrada = 46; break;
        case 'V' : intCodigoSimboloEntrada = 47; break;
        case 'W' : intCodigoSimboloEntrada = 48; break;
        case 'X' : intCodigoSimboloEntrada = 49; break;
        case 'Y' : intCodigoSimboloEntrada = 50; break;
        case 'Z' : intCodigoSimboloEntrada = 51; break;
        case '0' : intCodigoSimboloEntrada = 52; break;
        case '1' : intCodigoSimboloEntrada = 53; break;
        case '2' : intCodigoSimboloEntrada = 54; break;
        case '3' : intCodigoSimboloEntrada = 55; break;
        case '4' : intCodigoSimboloEntrada = 56; break;
        case '5' : intCodigoSimboloEntrada = 57; break;
        case '6' : intCodigoSimboloEntrada = 58; break;
        case '7' : intCodigoSimboloEntrada = 59; break;
        case '8' : intCodigoSimboloEntrada = 60; break;
        case '9' : intCodigoSimboloEntrada = 61; break;
        case '_' : intCodigoSimboloEntrada = 62; break;
        case '{' : intCodigoSimboloEntrada = 63; break;
        case '}' : intCodigoSimboloEntrada = 64; break;
        case '=' : intCodigoSimboloEntrada = 65; break;
        case '(' : intCodigoSimboloEntrada = 66; break;
        case ')' : intCodigoSimboloEntrada = 67; break;
        case '+' : intCodigoSimboloEntrada = 68; break;
        case '-' : intCodigoSimboloEntrada = 69; break;
        case '*' : intCodigoSimboloEntrada = 70; break;
        case '/' : intCodigoSimboloEntrada = 71; break;
        case '^' : intCodigoSimboloEntrada = 72; break;
        case '!' : intCodigoSimboloEntrada = 73; break;
        case '<' : intCodigoSimboloEntrada = 74; break;
        case '>' : intCodigoSimboloEntrada = 75; break;
        case ';' : intCodigoSimboloEntrada = 76; break;
        case '?' : intCodigoSimboloEntrada = 77; break;
        case '.' : intCodigoSimboloEntrada = 78; break;
        case ' ' : intCodigoSimboloEntrada = 79; break;         //ESP
        case '\t'  : intCodigoSimboloEntrada = 80; break;
        case '\n'  : intCodigoSimboloEntrada = 81; break;
        case -1  : intCodigoSimboloEntrada = 82; break;		//EOF
        default: break;
    }//switch

    return intCodigoSimboloEntrada;
}//ObtenerCodigoSimboloEntrada

ParserLL::Symbols Scanner::ObtenerEtiquetaDelSimboloTerminal(int intCodigoSimboloTerminal)
{
    switch(intCodigoSimboloTerminal)
    {
        case 1 : return ParserLL::TS_PROGRAMA;
        case 2 : return ParserLL::TS_LLAVE_I;
        case 3 : return ParserLL::TS_LLAVE_D;
        case 4 : return ParserLL::TS_PUNTO_COMA;
        case 5 : return ParserLL::TS_IGUAL;
        case 6 : return ParserLL::TS_DOBLE_IGUAL;
        case 7 : return ParserLL::TS_IF;
        case 8 : return ParserLL::TS_PARENT_I;
        case 9 : return ParserLL::TS_PARENT_D;
        case 10 : return ParserLL::TS_ELSE;
        case 11 : return ParserLL::TS_INTERROGACION;
        case 12 : return ParserLL::TS_ADMIRACION;
        case 13 : return ParserLL::TS_DIFERENTE;
        case 14 : return ParserLL::TS_MAS;
        case 15 : return ParserLL::TS_MENOS;
        case 16 : return ParserLL::TS_POR;
        case 17 : return ParserLL::TS_ENTRE;
        case 18 : return ParserLL::TS_MENOR;
        case 19 : return ParserLL::TS_MAYOR;
        case 20 : return ParserLL::TS_MENOR_IGUAL;
        case 21 : return ParserLL::TS_MAYOR_IGUAL;
        case 22 : return ParserLL::TS_POTENCIA;
        case 23 : return ParserLL::TS_NUM_ENTERO;
        case 24 : return ParserLL::TS_NUM_REAL;
        case 26 : return ParserLL::TS_WHILE;
        case 27 : return ParserLL::TS_ID;
        case 31 : return ParserLL::TS_EOS;
        default: return ParserLL::TS_ESP;
    }//switch
}//ObtenerEtiquetaDelSimboloTerminal

int Scanner::escanear(string strArchivo)
{
    FILE *ptrArchivoFuente;             //TIPO FILE POR EL getc de c/caracter
    int intCodigoSimboloEntrada;
    int intCodigoSimboloTerminal;
    int intEstadoSiguiente;
    int intTipoNodo;
    bool bolLeerSigCar = true;
    ParserLL::Symbols strTokenSimboloTerminal;

    ptrArchivoFuente = fopen(strArchivo.c_str(), "r");

    if (ptrArchivoFuente==NULL)
        cout<<"ERROR EN UN ARCHIVO FUENTE"<<endl;
    else
    {
        int intEstadoActual = 0;
        char c;
        do
        {
            if (bolLeerSigCar)
            {
                c = getc(ptrArchivoFuente);
            }
            else
                bolLeerSigCar = true;
            
            intCodigoSimboloEntrada = ObtenerCodigoSimboloEntrada(c);
            intEstadoSiguiente = MT[intEstadoActual][intCodigoSimboloEntrada];
            intTipoNodo = MT[intEstadoActual][AUTOMATA_TIPO_NODO];
             //Si se ha reconocido un token del lenguaje
            if ((intEstadoSiguiente == SCANNER_TOKEN_RECONOCIDO) && ((intTipoNodo == TIPONODO_FINAL) ||
                    (intTipoNodo == TIPONODO_INTERMEDIO_FINAL)))
            {   //Se ha reconocido un token
                intCodigoSimboloTerminal = MT[intEstadoActual][AUTOMATA_CODIGO_SALIDA];
                strTokenSimboloTerminal = ObtenerEtiquetaDelSimboloTerminal(intCodigoSimboloTerminal);
                //guardar token reconocido
                if(strTokenSimboloTerminal != ParserLL::TS_ESP)
                        TokensReconocidos.push_back(strTokenSimboloTerminal);
                strTokenSimboloTerminal = ParserLL::TS_ESP;
                intEstadoActual = 0;
                bolLeerSigCar = false;
            }
            else if (intEstadoSiguiente == SCANNER_ERROR)
            {
                //Se ha cometido un error l�xico
                cout << c << "Error Lexico" << endl;
                //break; //del do-while
            }
            else
            {
                //Sigo reconociendo dentro del autómata
                intEstadoActual = intEstadoSiguiente;
            }
        //end;
        }while (c != EOF);
        TokensReconocidos.push_back(ParserLL::TS_EOS);
    }
    return 0;
}//scanner

vector<ParserLL::Symbols> Scanner::getVectorTokensReconocidos()
{
    return TokensReconocidos;
}

