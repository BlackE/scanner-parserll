/* 
 * File:   ParserLL.h
 * Author: warlock
 *
 * Created on April 11, 2013, 12:44 PM
 */

#ifndef PARSERLL_H
#define	PARSERLL_H

#include <vector>
#include <string>

class ParserLL {
public:
    enum Symbols {
        // the symbols:
        // Terminal symbols:
        TS_PROGRAMA,
        TS_LLAVE_I,
        TS_LLAVE_D,
        TS_ID,
        TS_IF,
        TS_WHILE,
        TS_INTERROGACION,
        TS_PUNTO_COMA,
        TS_ELSE,
        TS_NUM_ENTERO,
        TS_NUM_REAL,
        TS_PARENT_I,
        TS_PARENT_D,
        TS_ADMIRACION,
        TS_AND,
        TS_OR,
        TS_MAYOR,
        TS_MENOR,
        TS_MAYOR_IGUAL,
        TS_MENOR_IGUAL,
        TS_DOBLE_IGUAL,
        TS_DIFERENTE,
        TS_MAS,
        TS_MENOS,
        TS_POR,
        TS_ENTRE,
        TS_POTENCIA,
        TS_EOS,
        TS_IGUAL,
        TS_ESP,
        // Non-terminal symbols:
        NTS_S,
        NTS_INSTR_COMPUESTA,
        NTS_BLOQUE_INSTR,
        NTS_BLOQUE_INSTR2,
        NTS_INSTR,
        NTS_INSTR_ASIGNACION,
        NTS_INSTR_IF,
        NTS_INSTR_IF2,
        NTS_INSTR_WHILE,
        NTS_INSTR_IMPRIME,
        NTS_EXP,
        NTS_EXP2,
        NTS_EXP_RELACIONAL,
        NTS_EXP_RELACIONAL2,
        NTS_EXP_COMPUESTA,
        NTS_EXP_COMPUESTA2,
        NTS_EXP_SIMPLE,
        NTS_EXP_SIMPLE2,
        NTS_FACTOR,
        NTS_FACTOR2,
        NTS_TERMINO,
        NTS_OP_ADITIVO,
        NTS_OP_MULTIPLICATIVO,
        NTS_OP_RELACIONAL,
        NTS_OP_PON,
        NTS_NUMERO,
        NTS_OP_LOGICO
    };
    ParserLL(std::vector<enum Symbols> tokenStack);
private:
    std::string getEnumLabel(enum Symbols simbolo);
    void printTokenStack(std::vector<enum Symbols> tokenStack);
};

#endif	/* PARSERLL_H */

