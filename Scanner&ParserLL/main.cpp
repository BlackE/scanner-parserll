/* 
 * File:   main.cpp
 * Author: blacke
 *
 * Created on April 11, 2013, 12:25 PM
 */

#include "Scanner.h"
#include "ParserLL.h"
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    Scanner prueba;
    string algo = "text.txt";
    prueba.escanear(algo);
    
    ParserLL parser = ParserLL(prueba.getVectorTokensReconocidos());
    return 0;
}

