/* 
 * File:   ParserLL.cpp
 * Author: warlock
 * 
 * Created on April 11, 2013, 12:44 PM
 */

#include "ParserLL.h"
#include <map>
#include <stack>
#include <iostream>
#include <vector>

using namespace std;

ParserLL::ParserLL(vector<enum Symbols> tokenStack) {
    
    // LL parser table, maps < non-terminal, terminal> pair to action       
    map< enum Symbols, map<enum Symbols, int> > table; 
    stack<enum Symbols> symbolStack;    // symbol stack
    int stackIdx = 0;
    
    // initialize the symbols stack
     symbolStack.push(TS_EOS);        // terminal, $
     symbolStack.push(NTS_S);         // non-terminal, S

    // setup the parsing table
    table[NTS_S][TS_PROGRAMA]                   = 1;
    table[NTS_INSTR_COMPUESTA][TS_LLAVE_I]      = 2;
    table[NTS_BLOQUE_INSTR][TS_LLAVE_I]         = 3;
    table[NTS_BLOQUE_INSTR][TS_ID]              = 3;
    table[NTS_BLOQUE_INSTR][TS_IF]              = 3;
    table[NTS_BLOQUE_INSTR][TS_WHILE]           = 3;
    table[NTS_BLOQUE_INSTR][TS_INTERROGACION]   = 3;
    table[NTS_BLOQUE_INSTR2][TS_PUNTO_COMA]     = 4;
    table[NTS_BLOQUE_INSTR2][TS_LLAVE_D]        = 5;
    table[NTS_INSTR][TS_ID]                     = 6;
    table[NTS_INSTR][TS_IF]                     = 7;
    table[NTS_INSTR][TS_WHILE]                  = 8;
    table[NTS_INSTR][TS_INTERROGACION]          = 9;
    table[NTS_INSTR][TS_LLAVE_I]                = 10;
    table[NTS_INSTR_ASIGNACION][TS_ID]          = 11;
    table[NTS_INSTR_IF][TS_IF]                  = 12;
    table[NTS_INSTR_IF2][TS_ELSE]               = 13;
    table[NTS_INSTR_IF2][TS_LLAVE_D]            = 14;
    table[NTS_INSTR_IF2][TS_PUNTO_COMA]         = 14;
    table[NTS_INSTR_WHILE][TS_WHILE]            = 15;
    table[NTS_INSTR_IMPRIME][TS_INTERROGACION]  = 16;
    table[NTS_EXP][TS_ID]                       = 17;
    table[NTS_EXP][TS_NUM_ENTERO]               = 17;
    table[NTS_EXP][TS_NUM_REAL]                 = 17;
    table[NTS_EXP][TS_PARENT_I]                 = 17;
    table[NTS_EXP][TS_ADMIRACION]               = 17;
//    table[NTS_EXP][TS_PUNTO_COMA]               = 17;
    
    table[NTS_EXP2][TS_AND]                     = 18;
    table[NTS_EXP2][TS_OR]                      = 18;
    table[NTS_EXP2][TS_LLAVE_D]                 = 19;
    table[NTS_EXP2][TS_PUNTO_COMA]              = 19;
    table[NTS_EXP2][TS_ELSE]                    = 19;
    table[NTS_EXP2][TS_PARENT_D]                = 19;
    table[NTS_EXP_RELACIONAL][TS_ID]            = 20;
    table[NTS_EXP_RELACIONAL][TS_NUM_ENTERO]    = 20;
    table[NTS_EXP_RELACIONAL][TS_NUM_REAL]      = 20;
    table[NTS_EXP_RELACIONAL][TS_PARENT_I]      = 20;
    table[NTS_EXP_RELACIONAL][TS_ADMIRACION]    = 20;
    table[NTS_EXP_RELACIONAL2][TS_MAYOR]        = 21;
    table[NTS_EXP_RELACIONAL2][TS_MENOR]        = 21;
    table[NTS_EXP_RELACIONAL2][TS_MAYOR_IGUAL]  = 21;
    table[NTS_EXP_RELACIONAL2][TS_MENOR_IGUAL]  = 21;
    table[NTS_EXP_RELACIONAL2][TS_DOBLE_IGUAL]  = 21;
    table[NTS_EXP_RELACIONAL2][TS_DIFERENTE]    = 21;
    table[NTS_EXP_RELACIONAL2][TS_LLAVE_D]      = 22;
    table[NTS_EXP_RELACIONAL2][TS_PUNTO_COMA]   = 22;
    table[NTS_EXP_RELACIONAL2][TS_ELSE]         = 22;
    table[NTS_EXP_RELACIONAL2][TS_PARENT_D]     = 22;
    table[NTS_EXP_RELACIONAL2][TS_AND]          = 22;
    table[NTS_EXP_RELACIONAL2][TS_OR]           = 22;
    table[NTS_EXP_COMPUESTA][TS_ID]             = 23;
    table[NTS_EXP_COMPUESTA][TS_NUM_ENTERO]     = 23;
    table[NTS_EXP_COMPUESTA][TS_NUM_REAL]       = 23;
    table[NTS_EXP_COMPUESTA][TS_PARENT_I]       = 23;
    table[NTS_EXP_COMPUESTA][TS_ADMIRACION]     = 23;
    table[NTS_EXP_COMPUESTA2][TS_MAS]           = 24;
    table[NTS_EXP_COMPUESTA2][TS_MENOS]         = 24;
    table[NTS_EXP_COMPUESTA2][TS_LLAVE_D]       = 25;
    table[NTS_EXP_COMPUESTA2][TS_PUNTO_COMA]    = 25;
    table[NTS_EXP_COMPUESTA2][TS_ELSE]          = 25;
    table[NTS_EXP_COMPUESTA2][TS_PARENT_D]      = 25;
    table[NTS_EXP_COMPUESTA2][TS_AND]           = 25;
    table[NTS_EXP_COMPUESTA2][TS_OR]            = 25;
    table[NTS_EXP_COMPUESTA2][TS_MAYOR]         = 25;
    table[NTS_EXP_COMPUESTA2][TS_MENOR]         = 25;
    table[NTS_EXP_COMPUESTA2][TS_MAYOR_IGUAL]   = 25;
    table[NTS_EXP_COMPUESTA2][TS_MENOR_IGUAL]   = 25;
    table[NTS_EXP_COMPUESTA2][TS_DOBLE_IGUAL]   = 25;
    table[NTS_EXP_COMPUESTA2][TS_DIFERENTE]     = 25;
    table[NTS_EXP_SIMPLE][TS_ID]                = 26;
    table[NTS_EXP_SIMPLE][TS_NUM_ENTERO]        = 26;
    table[NTS_EXP_SIMPLE][TS_NUM_REAL]          = 26;
    table[NTS_EXP_SIMPLE][TS_PARENT_I]          = 26;
    table[NTS_EXP_SIMPLE][TS_ADMIRACION]        = 26;
    table[NTS_EXP_SIMPLE2][TS_POR]              = 27;
    table[NTS_EXP_SIMPLE2][TS_ENTRE]            = 27;
    table[NTS_EXP_SIMPLE2][TS_AND]              = 28;
    table[NTS_EXP_SIMPLE2][TS_OR]               = 28;
    table[NTS_EXP_SIMPLE2][TS_MAYOR]            = 28;
    table[NTS_EXP_SIMPLE2][TS_MENOR]            = 28;
    table[NTS_EXP_SIMPLE2][TS_MAYOR_IGUAL]      = 28;
    table[NTS_EXP_SIMPLE2][TS_MENOR_IGUAL]      = 28;
    table[NTS_EXP_SIMPLE2][TS_DOBLE_IGUAL]      = 28;
    table[NTS_EXP_SIMPLE2][TS_DIFERENTE]        = 28;
    table[NTS_EXP_SIMPLE2][TS_MAS]              = 28;
    table[NTS_EXP_SIMPLE2][TS_MENOS]            = 28;
    table[NTS_EXP_SIMPLE2][TS_LLAVE_D]          = 28;
    table[NTS_EXP_SIMPLE2][TS_PUNTO_COMA]       = 28;
    table[NTS_EXP_SIMPLE2][TS_ELSE]             = 28;
    table[NTS_EXP_SIMPLE2][TS_PARENT_D]         = 28;
    table[NTS_FACTOR][TS_ID]                    = 29;
    table[NTS_FACTOR][TS_NUM_ENTERO]            = 29;
    table[NTS_FACTOR][TS_NUM_REAL]              = 29;
    table[NTS_FACTOR][TS_PARENT_I]              = 29;
    table[NTS_FACTOR][TS_ADMIRACION]            = 29;
    table[NTS_FACTOR2][TS_POTENCIA]             = 30;
    table[NTS_FACTOR2][TS_AND]                  = 31;
    table[NTS_FACTOR2][TS_OR]                   = 31;
    table[NTS_FACTOR2][TS_MAYOR]                = 31;
    table[NTS_FACTOR2][TS_MENOR]                = 31;
    table[NTS_FACTOR2][TS_MAYOR_IGUAL]          = 31;
    table[NTS_FACTOR2][TS_MENOR_IGUAL]          = 31;
    table[NTS_FACTOR2][TS_DOBLE_IGUAL]          = 31;
    table[NTS_FACTOR2][TS_DIFERENTE]            = 31;
    table[NTS_FACTOR2][TS_MAS]                  = 31;
    table[NTS_FACTOR2][TS_MENOS]                = 31;
    table[NTS_FACTOR2][TS_POR]                  = 31;
    table[NTS_FACTOR2][TS_ENTRE]                = 31;
    table[NTS_FACTOR2][TS_LLAVE_D]              = 31;
    table[NTS_FACTOR2][TS_PUNTO_COMA]           = 31;
    table[NTS_FACTOR2][TS_ELSE]                 = 31;
    table[NTS_FACTOR2][TS_PARENT_D]             = 31;
    table[NTS_TERMINO][TS_NUM_ENTERO]           = 32;
    table[NTS_TERMINO][TS_NUM_REAL]             = 32;
    table[NTS_TERMINO][TS_ID]                   = 33;
    table[NTS_TERMINO][TS_PARENT_I]             = 34;
    table[NTS_TERMINO][TS_ADMIRACION]           = 35;
    table[NTS_OP_ADITIVO][TS_MAS]               = 36;
    table[NTS_OP_ADITIVO][TS_MENOS]             = 37;
    table[NTS_OP_MULTIPLICATIVO][TS_POR]        = 38;
    table[NTS_OP_MULTIPLICATIVO][TS_ENTRE]      = 39;
    table[NTS_OP_RELACIONAL][TS_MAYOR]          = 40;
    table[NTS_OP_RELACIONAL][TS_MENOR]          = 41;
    table[NTS_OP_RELACIONAL][TS_MAYOR_IGUAL]    = 42;
    table[NTS_OP_RELACIONAL][TS_MENOR_IGUAL]    = 43;
    table[NTS_OP_RELACIONAL][TS_DOBLE_IGUAL]    = 44;
    table[NTS_OP_RELACIONAL][TS_DIFERENTE]      = 45;
    table[NTS_OP_PON][TS_POTENCIA]              = 46;
    table[NTS_NUMERO][TS_NUM_ENTERO]            = 47;
    table[NTS_NUMERO][TS_NUM_REAL]              = 48;
    table[NTS_OP_LOGICO][TS_AND]                = 49;
    table[NTS_OP_LOGICO][TS_OR]                 = 50;
    
    printTokenStack(tokenStack);

    while(symbolStack.size() > 0)
    {
        if(tokenStack[stackIdx] == symbolStack.top())
        {
            cout << "Simbolos iguales: " << getEnumLabel(tokenStack[stackIdx]) << endl;
            stackIdx++;
            symbolStack.pop();
        }
        else
        {
            cout << "Regla " << table[symbolStack.top()][tokenStack[stackIdx]] << endl;
            switch(table[symbolStack.top()][tokenStack[stackIdx]])
            {
                case 1: //1. S → “programa” InstruccionCompuesta
                    cout << "S → “programa” InstruccionCompuesta" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_INSTR_COMPUESTA);
		    symbolStack.push(TS_PROGRAMA);
                    break;
                case 2:	//2. InstruccionCompuesta → “{“ BloqueInstrucciones “}” 
                    cout << "InstruccionCompuesta → “{“ BloqueInstrucciones “}”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_LLAVE_D);
		    symbolStack.push(NTS_BLOQUE_INSTR);
		    symbolStack.push(TS_LLAVE_I);
                    break;
                case 3: //3. BloqueInstrucciones -> Instrucción BloqueInstrucciones2
                    cout << "BloqueInstrucciones -> Instrucción BloqueInstrucciones2" << endl;
                    symbolStack.pop();  
		    symbolStack.push(NTS_BLOQUE_INSTR2); 
		    symbolStack.push(NTS_INSTR);
                    break;
                case 4: //4. BloqueInstrucciones2 -> “;” Instrucción BloqueInstrucciones2
                    cout << "BloqueInstrucciones2 -> “;” Instrucción BloqueInstrucciones2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_BLOQUE_INSTR2); 
		    symbolStack.push(NTS_INSTR); 
                    symbolStack.push(TS_PUNTO_COMA);
                    break;
                case 5:  //5. BloqueInstrucciones2 -> ε
                    cout << "BloqueInstrucciones2 -> ε" << endl;
                    symbolStack.pop();
                    break;
                case 6: //6. Instruccion -> InstruccionAsignacion
                    cout << "Instruccion -> InstruccionAsignacion" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_INSTR_ASIGNACION);
                    break;
                case 7: //7. Instruccion -> InstruccionIf
                    cout << "Instruccion -> InstruccionIf" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_INSTR_IF);  
                    break;
		    
                case 8: //8.Instruccion -> InstruccionWhile
                    cout << "Instruccion -> InstruccionWhile" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_INSTR_WHILE); 
                    break;
                case 9: //9. Instruccion -> InstruccionImprime
                    cout << "Instruccion -> InstruccionImprime" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_INSTR_IMPRIME); 
                    break;
                case 10: //10. Instruccion -> InstruccionCompuesta
                    cout << "Instruccion -> InstruccionCompuesta" << endl;
                    symbolStack.pop();
                    symbolStack.push(NTS_INSTR_COMPUESTA);
                    break;
                case 11:  //11.InstruccionAsignacion ->  ID “=” Expresion
                    cout << "InstruccionAsignacion ->  ID “=” Expresion" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP);
		    symbolStack.push(TS_IGUAL);
		    symbolStack.push(TS_ID);
                    break;
                case 12: //12.InstruccionIf ->  “if” “(“ Expresion “)” Instruccion InstruccionIf2
                    cout << "InstruccionIf ->  “if” “(“ Expresion “)” Instruccion InstruccionIf2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_INSTR_IF2);
		    symbolStack.push(NTS_INSTR);
		    symbolStack.push(TS_PARENT_D);
		    symbolStack.push(NTS_EXP);
		    symbolStack.push(TS_PARENT_I);
		    symbolStack.push(TS_IF);
                    break;
                case 13: //13.InstruccionIf2 -> “else” Instrucción
                    cout << "InstruccionIf2 -> “else” Instrucción" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_INSTR);
		    symbolStack.push(TS_ELSE);
                    break;
                case 14: //14.InstruccionIf2 -> ε
                    cout << "InstruccionIf2 -> ε" << endl;
                    symbolStack.pop();
                    break;
                case 15: //15. InstruccionWhile ->  “while” “(“ Expresion “)” Instrucción
                    cout << "InstruccionWhile ->  “while” “(“ Expresion “)” Instrucción" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_INSTR);
		    symbolStack.push(TS_LLAVE_D);
		    symbolStack.push(NTS_EXP);
		    symbolStack.push(TS_LLAVE_I);
		    symbolStack.push(TS_WHILE);
                    break;
                case 16: //16.InstruccionImprime ->  “?” Expresion
                    cout << "InstruccionImprime ->  “?” Expresion" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP);
		    symbolStack.push(TS_INTERROGACION);
                    break;
                case 17: //17. Expresion ->  ExpRelacional ExpRelacional2
                    cout << "Expresion ->  ExpRelacional ExpRelacional2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP_RELACIONAL2);
		    symbolStack.push(NTS_EXP_RELACIONAL);
                    break;
                case 18: //18.ExpRelacional2 ->  OpLogico ExpRelacional ExpRelacional2
                    cout << "ExpRelacional2 ->  OpLogico ExpRelacional ExpRelacional2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP_RELACIONAL2);
		    symbolStack.push(NTS_EXP_RELACIONAL);
		    symbolStack.push(NTS_OP_LOGICO);
                    break;
                case 19: //19.ExpRelacional2 -> ε
                    cout << "ExpRelacional2 -> ε" << endl;
                    symbolStack.pop();
                    break;
                case 20: //20. ExpRelacional ->  ExpCompuesta ExpRelacional2
                    cout << "ExpRelacional ->  ExpCompuesta ExpRelacional2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP_RELACIONAL2);
		    symbolStack.push(NTS_EXP_COMPUESTA);
                    break;
                case 21: //21.ExpRelacional2 ->  OpRelacional ExpCompuesta ExpRelacional2
                    cout << "ExpRelacional2 ->  OpRelacional ExpCompuesta ExpRelacional2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP_RELACIONAL2);
		    symbolStack.push(NTS_EXP_COMPUESTA);
		    symbolStack.push(NTS_OP_RELACIONAL);
                    break;
                case 22: //ExpRelacional2 ->  ε
                    cout << "ExpRelacional2 ->  ε" << endl;
                    symbolStack.pop();
                    break;
                case 23: //ExpCompuesta ->  ExpSimple ExpCompuesta2
                    cout << "ExpCompuesta ->  ExpSimple ExpCompuesta2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP_COMPUESTA2);
		    symbolStack.push(NTS_EXP_SIMPLE);
                    break;
                case 24: //ExpCompuesta2 ->  OpAditivo ExpSimple ExpCompuesta2
                    cout << "ExpCompuesta2 ->  OpAditivo ExpSimple ExpCompuesta2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP_COMPUESTA2);
		    symbolStack.push(NTS_EXP_SIMPLE);
                    symbolStack.push(NTS_OP_ADITIVO);
		    break;
                case 25: //ExpCompuesta2 -> ε
                    cout << "ExpCompuesta2 -> ε" << endl;
                    symbolStack.pop();
                    break;
                case 26: //ExpSimple ->  Factor ExpSimple2
                    cout << "ExpSimple ->  Factor ExpSimple2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP_SIMPLE2);
		    symbolStack.push(NTS_FACTOR);
                    break;
                case 27: //ExpSimple2 ->  OpMultiplicativo Factor ExpSimple2
                    cout << "ExpSimple2 ->  OpMultiplicativo Factor ExpSimple2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_EXP_SIMPLE2);
		    symbolStack.push(NTS_FACTOR);
		    symbolStack.push(NTS_OP_MULTIPLICATIVO);
                    break;
                case 28: //ExpSimple2 ->  ε
                    cout << "ExpSimple2 ->  ε" << endl;
                    symbolStack.pop();
                    break;
                case 29: //Factor ->  Termino Factor2
                    cout << "Factor ->  Termino Factor2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_FACTOR2);
		    symbolStack.push(NTS_TERMINO);
                    break;
                case 30: //Factor2 ->  OpPon Termino Factor2
                    cout << "Factor2 ->  OpPon Termino Factor2" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_FACTOR2);
		    symbolStack.push(NTS_TERMINO);
		    symbolStack.push(NTS_OP_PON);
                    break;
                case 31: //Factor2 ->  ε
                    cout << "Factor2 ->  ε" << endl;
                    symbolStack.pop();
                    break;
                case 32: //Termino ->  Numero
                    cout << "Termino ->  Numero" << endl;
                    symbolStack.pop();
		    symbolStack.push(NTS_NUMERO);
                    break;
		case 33: //Termino ->  ID
                    cout << "Termino ->  ID" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_ID);
                    break;
		case 34: //Termino ->  “(“ Expresion “)”
                    cout << "Termino ->  “(“ Expresion “)”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_PARENT_D);
		    symbolStack.push(NTS_EXP);
		    symbolStack.push(TS_PARENT_I);
                    break;
		case 35: //Termino ->  “!” “(“ Expresion “)”
                    cout << "Termino ->  “!” “(“ Expresion “)”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_PARENT_D);
		    symbolStack.push(NTS_EXP);
		    symbolStack.push(TS_PARENT_I);
		    symbolStack.push(TS_ADMIRACION);
                    break;
		case 36: //OpAditivo -> “+”
                    cout << "OpAditivo -> “+”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_MAS);
		    break;
		case 37: //OpAditivo -> “-“
                    cout << "OpAditivo -> “-“" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_MENOS); 
                    break;		    
		case 38: //OpMultiplicativo ->  “*”
                    cout << "OpMultiplicativo ->  “*”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_POR);
                    break;
		case 39: //OpMultiplicativo ->  “/”
                    cout << "OpMultiplicativo ->  “/”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_ENTRE);
                    break;
		case 40: //OpRelacional ->  “>”
                    cout << "OpRelacional ->  “>”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_MAYOR);
		    break;
		case 41: //OpRelacional -> ”<”
                    cout << "OpRelacional -> ”<”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_MENOR);
                    break;
		case 42: //OpRelacional -> ”>=”
                    cout << "OpRelacional -> ”>=”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_MAYOR_IGUAL);
                    break;
		case 43: //OpRelacional -> ”<=”
                    cout << "OpRelacional -> ”<=”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_MENOR_IGUAL);
                    break;
		case 44: //OpRelacional -> ”==”
                    cout << "OpRelacional -> ”==”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_DOBLE_IGUAL);
                    break;
		case 45: //OpRelacional -> ”!=”
                    cout << "OpRelacional -> ”!=”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_DIFERENTE);
                    break;
		case 46: //OpPon ->  “^”
                    cout << "OpPon ->  “^”" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_POTENCIA);
                    break;
		case 47: //Numero ->  NumeroEntero
                    cout << "Numero ->  NumeroEntero" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_NUM_ENTERO);
                    break;
		case 48: //Numero ->  NumeroReal
                    cout << "Numero ->  NumeroReal" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_NUM_REAL);
                    break;
		case 49: //OpLogico ->  AND
                    cout << "OpLogico ->  AND" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_AND);
                    break;
		case 50: //OpLogico ->  OR
                    cout << "OpLogico ->  OR" << endl;
                    symbolStack.pop();
		    symbolStack.push(TS_OR);
                    break;
                default:
                    cout << "Parsing table defaulted with: " << getEnumLabel(symbolStack.top());
                    cout << " " << getEnumLabel(tokenStack[stackIdx]) << endl;
                    return;
            }
        }
    }

    cout << "Finished parsing" << endl;

    return;
}

string ParserLL::getEnumLabel(Symbols simbolo) {
    switch(simbolo) {
        case TS_PROGRAMA: return string("TS_PROGRAMA");
        case TS_LLAVE_I: return string("TS_LLAVE_I");
        case TS_LLAVE_D: return string("TS_LLAVE_D");
        case TS_ID: return string("TS_ID");
        case TS_IF: return string("TS_IF");
        case TS_WHILE: return string("TS_WHILE");
        case TS_INTERROGACION: return string("TS_INTERROGACION");
        case TS_PUNTO_COMA: return string("TS_PUNTO_COMA");
        case TS_ELSE: return string("TS_ELSE");
        case TS_NUM_ENTERO: return string("TS_NUM_ENTERO");
        case TS_NUM_REAL: return string("TS_NUM_REAL");
        case TS_PARENT_I: return string("TS_PARENT_I");
        case TS_PARENT_D: return string("TS_PARENT_D");
        case TS_ADMIRACION: return string("TS_ADMIRACION");
        case TS_AND: return string("TS_AND");
        case TS_OR: return string("TS_OR");
        case TS_MAYOR: return string("TS_MAYOR");
        case TS_MENOR: return string("TS_MENOR");
        case TS_MAYOR_IGUAL: return string("TS_MAYOR_IGUAL");
        case TS_MENOR_IGUAL: return string("TS_MENOR_IGUAL");
        case TS_DOBLE_IGUAL: return string("TS_DOBLE_IGUAL");
        case TS_DIFERENTE: return string("TS_DIFERENTE");
        case TS_MAS: return string("TS_MAS");
        case TS_MENOS: return string("TS_MENOS");
        case TS_POR: return string("TS_POR");
        case TS_ENTRE: return string("TS_ENTRE");
        case TS_POTENCIA: return string("TS_POTENCIA");
        case TS_EOS: return string("TS_EOS");
        case TS_IGUAL: return string("TS_IGUAL");
        case TS_ESP: return string("TS_ESP");
        // Non-terminal symbols:
        case NTS_S: return string("NTS_S");
        case NTS_INSTR_COMPUESTA: return string("NTS_INSTR_COMPUESTA");
        case NTS_BLOQUE_INSTR: return string("NTS_BLOQUE_INSTR");
        case NTS_BLOQUE_INSTR2: return string("NTS_BLOQUE_INSTR2");
        case NTS_INSTR: return string("NTS_INSTR");
        case NTS_INSTR_ASIGNACION: return string("NTS_INSTR_ASIGNACION");
        case NTS_INSTR_IF: return string("NTS_INSTR_IF");
        case NTS_INSTR_IF2: return string("NTS_INSTR_IF2");
        case NTS_INSTR_WHILE: return string("NTS_INSTR_WHILE");
        case NTS_INSTR_IMPRIME: return string("NTS_INSTR_IMPRIME");
        case NTS_EXP: return string("NTS_EXP");
        case NTS_EXP2: return string("NTS_EXP2");
        case NTS_EXP_RELACIONAL: return string("NTS_EXP_RELACIONAL");
        case NTS_EXP_RELACIONAL2: return string("NTS_EXP_RELACIONAL2");
        case NTS_EXP_COMPUESTA: return string("NTS_EXP_COMPUESTA");
        case NTS_EXP_COMPUESTA2: return string("NTS_EXP_COMPUESTA2");
        case NTS_EXP_SIMPLE: return string("NTS_EXP_SIMPLE");
        case NTS_EXP_SIMPLE2: return string("NTS_EXP_SIMPLE2");
        case NTS_FACTOR: return string("NTS_FACTOR");
        case NTS_FACTOR2: return string("NTS_FACTOR2");
        case NTS_TERMINO: return string("NTS_TERMINO");
        case NTS_OP_ADITIVO: return string("NTS_OP_ADITIVO");
        case NTS_OP_MULTIPLICATIVO: return string("NTS_OP_MULTIPLICATIVO");
        case NTS_OP_RELACIONAL: return string("NTS_OP_RELACIONAL");
        case NTS_OP_PON: return string("NTS_OP_PON");
        case NTS_NUMERO: return string("NTS_NUMERO");
        case NTS_OP_LOGICO: return string("NTS_OP_LOGICO");
    }
}

void ParserLL::printTokenStack(vector<Symbols> tokenStack) {
    for(int idxStack = 0; idxStack < tokenStack.size(); idxStack++) {
        cout << getEnumLabel(tokenStack.at(idxStack)) << endl; 
    }
}