/* 
 * File:   Scanner.h
 * Author: blacke
 *
 * Created on April 11, 2013, 12:40 PM
 */

#include "ParserLL.h"
#include <iostream>
#include <string>
#include <vector>

//#pragma once 

#ifndef SCANNER_H
#define	SCANNER_H


using namespace std;

class Scanner {
public:
    int escanear(string strArchivo);
    vector<ParserLL::Symbols> getVectorTokensReconocidos(); 
private:
    vector<ParserLL::Symbols> TokensReconocidos;
    int ObtenerCodigoSimboloEntrada(int intASCIISimboloEntrada);
    ParserLL::Symbols ObtenerEtiquetaDelSimboloTerminal(int intCodigoSimboloTerminal);
};

#endif	/* SCANNER_H */

